import ExpenseItem from './ExpenseItem';
import './Expenses.css'

export default function Expenses(props) {

    return (
        <div className='expenses'>
            <ExpenseItem title={props.data[0].title} price={props.data[0].price} date={props.data[0].date} />
            <ExpenseItem title={props.data[1].title} price={props.data[1].price} date={props.data[1].date} />
            <ExpenseItem title={props.data[0].title} price={props.data[0].price} date={props.data[0].date} />
            <ExpenseItem title={props.data[1].title} price={props.data[1].price} date={props.data[1].date} />
        </div>
    );
}