// import logo from './logo.svg';
// import './App.css';

import ExpenseItem from "./components/ExpenseItem";
import Expenses from "./components/Expenses";

function App() {
  const data  = [
    {
      'id' : 1,
      'title' : 'car insurance',
      'price' : 299.2,
      'date' : new Date(2022,1,1)
    },
    {
      'id' : 2,
      'title' : 'home insurance',
      'price' : 2000,
      'date' : new Date(2024,3,22)
    },
    {
      'id' : 3,
      'title' : 'job insurance',
      'price' : 3000,
      'date' : new Date(2025,9,13)
    },
    {
      'id' : 4,
      'title' : 'life insurance',
      'price' : 10000,
      'date' : new Date(2028,5,16)
    }
  ];
  return (
    <div>
    <Expenses data={data} />
    </div>
  );
}

export default App;
